﻿using System;

namespace ShiftArrayElements
{
    public static class Shifter
    {
        public static int[] Shift(int[] source, int[] iterations)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations is null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            for (int i = 0; i < iterations.Length; i++)
            {
                if (i % 2 == 0)
                {
                    for (int k = 0; k < iterations[i]; k++)
                    {
                        var temp2 = source[0];
                        Array.Copy(source, 1, source, 0, source.Length - 1);
                        source[^1] = temp2;
                    }
                }
                else
                {
                    for (int j = 0; j < iterations[i]; j++)
                    {
                        var temp = source[^1];
                        Array.Copy(source, 0, source, 1, source.Length - 1);
                        source[0] = temp;
                    }
                }
            }

            return source;
        }
    }
}
