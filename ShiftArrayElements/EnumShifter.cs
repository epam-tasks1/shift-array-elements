﻿using System;

namespace ShiftArrayElements
{
    public static class EnumShifter
    {
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions is null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            for (int i = 0; i < directions.Length; i++)
            {
                switch (directions[i])
                {
                    case Direction.Left:

                        int tmpLEFT;

                        for (int k = 0; k < source.Length - 1; k++)
                        {
                            tmpLEFT = source[k];
                            source[k] = source[k + 1];
                            source[k + 1] = tmpLEFT;
                        }

                        continue;

                    case Direction.Right:

                        int tmpRIGHT;

                        for (int j = source.Length - 1; j > 0; j--)
                        {
                            tmpRIGHT = source[j];
                            source[j] = source[j - 1];
                            source[j - 1] = tmpRIGHT;
                        }

                        continue;

                    default:
                        throw new InvalidOperationException($"Incorrect {directions[i]} enum value.");
                }
            }

            return source;
        }
    }
}
